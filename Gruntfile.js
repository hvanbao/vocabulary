// Gruntfile.js
// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt){
	// CONFIGURE GRUNT =======================================
	grunt.initConfig({
		// get the configuration info from package.json ---------
		// this way we can use things like name and version (pkg.name)
		pkg: grunt.file.readJSON("package.json"),
		// configure jshint to validate js files
		jshint: {
			option: {
				// use jshint-stylish to make our errors look and read good
				reporter: require('jshint-stylish')
			},
			// when this task is run, lint the Gruntfile and all js files in src
			build: ['Grunfile.js', 'src/js/*.js'],
			model: ['Grunfile.js', 'src/js/models/*.js'],
			model: ['Grunfile.js', 'src/js/collections/*.js'],
			view: ['Grunfile.js', 'src/js/views/*.js'],
			router: ['Grunfile.js', 'src/js/routers/*.js'],
			
		}
	});
	//Load the plugin that provides the "jshint" task
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.registerTask("default", ["jshint"]);
};