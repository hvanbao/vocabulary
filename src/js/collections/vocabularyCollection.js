define([
	"underscore",
	"backbone",
	"models/vocabularyModel"
], function(_, Backbone, VocabularyModel){
	//Vocabulary Collection
	var VocabularyCollection = Backbone.Collection.extend({
		model: VocabularyModel,
		//make a new "order" value and return it.
		nextOrder: function(){
			return this.length ? this.last().get("order") + 1 : 1;
		},
		//upload data to localStorage
		uploadLocalStorage: function(){
			localStorage.setItem("vocabulary", "");
			localStorage.setItem("vocabulary", JSON.stringify(this));
		},
		//download data from localStorage
		//and set this models = data
		downloadLocalStorage: function(store_name){
			var data = JSON.parse(localStorage.getItem(store_name));
			this.set(data);
		},
		//Filter
		//get active vocabularies
		//return list of Vocabulary have completed state are false
		getActiveItem: function(){
			//var list = this.downloadLocalStorage(storeName);
			var listActive = this.where({completed: false});
			return listActive;
		},
		//get completed vocabularies
		//return list of Vocabulary have completed state are true
		getCompletedItem: function(){
			var listCompleted = this.where({completed: true});
			return listCompleted;
		}
	});
	return VocabularyCollection;
});


