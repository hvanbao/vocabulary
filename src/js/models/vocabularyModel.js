define([
	"underscore",
	"backbone"
], function(_, Backbone){
	//This is base model when you want to create a model...
	var VocabularyModel = Backbone.Model.extend({
		//Defaults value when create new vocabulary
		defaults: {
			vocabulary: "",
			completed: false
		},
		// Toggle the `completed` state of this vocabulary.
		switchState: function(){
			this.set({
				completed: !this.get("completed")
			});
		},
		//change value of vocabulary.
		updateName: function(name){
			this.set({
				vocabulary: name
			});
		}
	});
	return VocabularyModel;
});


