define([
	"jquery",
	"underscore",
	"backbone",
	// Pull in the Collection module from above
	"collections/vocabularyCollection",
	// Pull in the View module from above
	"views/appViews",
	//Pull router module
	"routers/router"
], function($, _, Backbone, VocabularyCollection, AppView, Router){
	var initialize = function(){
		//declare a AppView
		app.appView = new AppView();
		//Create vocabularies collection
		app.vocabularies = new VocabularyCollection();
		app.vocabularies.downloadLocalStorage("vocabulary");
		//Create vocabularies has completed-collection
		app.vocabulariesCompleted = new VocabularyCollection();
		app.vocabulariesCompleted.set(app.vocabularies.getCompletedItem());
		//Create vocabularies active-collection
		app.vocabulariesActive = new VocabularyCollection();
		app.vocabulariesActive.set(app.vocabularies.getActiveItem());
		//this is update function which will update two vocabularyCollection in my app
		app.updateCollection = function(){
			app.vocabulariesCompleted.set(app.vocabularies.getCompletedItem());
			app.vocabulariesActive.set(app.vocabularies.getActiveItem());
		};
		// Pass in our Router module and call it's initialize function
		Router.initialize();
	};

	return {
		initialize: initialize
	};
});

