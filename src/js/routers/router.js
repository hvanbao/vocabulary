define([
	"jquery",
	"underscore",
	"backbone"
], function($, _, Backbone){
	// Vocabulary Router
	var AppRouter = Backbone.Router.extend({
		routes: {
			"": "index",
			"active": "getActive",
			"completed": "getCompleted"
		},
		//load view of # page (index)
		//this view use collection is app.vocabularies (all vocabulary)
		index: function(){
			this.incompleted = app.vocabularies.getActiveItem().length;
			if(app.listView){
				app.listView.remove();
			}
			app.appView.render(app.vocabularies);
		},
		//load view of #/completed page
		//this view use collection is app.vocabulariesCompleted 
		//(list vocabulary has completed state is true )
		getCompleted: function(){
			app.vocabulariesCompleted.set(app.vocabularies.getCompletedItem());
			if(app.listView){
				app.listView.remove();
			}
			app.appView.render(app.vocabulariesCompleted);
		},
		//load view of #/active page
		//this view use collection is app.vocabulariesActive 
		//(list vocabulary has completed state is false )
		getActive: function(){
			app.vocabulariesActive.set(app.vocabularies.getActiveItem());
			if(app.listView){
				app.listView.remove();
			}
			app.appView.render(app.vocabulariesActive);
		}
	});
	//Initialize: a AppRouter, Backbone history
	var initialize = function(){
		var app_router = new AppRouter();
		Backbone.history.start();
	};
	//return initialize function object
	return {
		initialize: initialize
	};
});

