//Global app variable
var app = app || {};
// define key value
var ENTER_KEY = 13;
var ESC_KEY = 27;
// Require.js allows us to configure shortcut alias
// in paths object we config four shortcut:
require.config({
	paths: {
		jquery: "libs/jquery/jquery",
		underscore: "libs/underscore/underscore",
		backbone: "libs/backbone/backbone",
		text: "libs/require/text"
	}
});
require(["appMain"], function(App){
//Call initialize function of appMain module
	 App.initialize();
});