define([
	"jquery",
	"underscore",
	"backbone",
	// Pull in the Model module from above
	"models/vocabularyModel",
	// Pull in the view module from above
	"views/vocabularyListView",
	// Pull in the templates module from above
	"text!templates/footerTemplate.html"
], function($, _, Backbone, VocabularyModel, VocabularyListView, footerTemplate){
	//main view, index page
	var AppView = Backbone.View.extend({
		el: $("#vocabularyApp"),
		//The DOM events specific to an element.
		events: {
			"keypress #new-vocabulary": "addOnEnter",
			"click #toggle-all": "toggleCompletedAll",
			"click #clear-completed":"clearCompleted"
		},
		//That is the template that will display when at least one vocabulary 
		//in the collection (app.vocabularies)
		template: _.template(footerTemplate),
		initialize: function(){
			//get some element by id
			// $ operator before "input" help me know it"s 
			// variable make by jQuery $
			this.$input = $("#new-vocabulary");
			this.$allCheckbox = $("#toggle-all")[0];
			this.$list = $("#vocabulary-list");
			this.$footer = $("#footer");
			this.$main = $("#main");
		},
		//Render view
		//"collecton" parameter is collection of this view
		render: function(collection){
			//get the number of elements Completed
			var complete = app.vocabularies.getCompletedItem().length;
			//get the number of elements Incompleted
			var incompleted = app.vocabularies.getActiveItem().length;
			//Hide or show element in html file to use
			if(app.vocabularies.length){
				this.$main.show();
				this.$footer.show();
				this.$footer.html(this.template({
					incompleted: incompleted,
					completed: complete
				}));
			}
			else{
				this.$main.hide();
				this.$footer.hide();
			}
			//Create List view with collection are "collection" parameter
			app.listView = new VocabularyListView({collection: collection});
			//If don't pass "" into this.$list.html(), 
			//when this render() recalled this.$list will not cleared,
			//so we have double list value
			this.$list.html("");
			this.$list.append(app.listView.render().el);
			//If all vocabulary has completed, $allCheckbox will checked.
			this.$allCheckbox.checked = !incompleted;
		},
		//change state of "completed" attribute all Item to "true"
		toggleCompletedAll: function(){
			var complete = this.$allCheckbox.checked;
			app.vocabularies.each(function(vocabulary){
				vocabulary.set({completed: complete});
			});
			app.updateCollection();
			this.render(app.listView.collection);
			app.vocabularies.uploadLocalStorage();
		},
		//triger when a new model has added to collection app.vocabularies
		addOne: function(vocabulary){
			var newView = new VocabularyView({model: vocabulary});
			this.$list.append(newView.render().el);
		},
		//Add a Vocabulary by click button Add new vocabulary
		addOnEnter: function(e){
			if(e.which === ENTER_KEY && this.$input.val().trim()){
				//Create new model contain a Vocabulary
				var newVocabulary = new VocabularyModel();
				newVocabulary.set({
					vocabulary: this.$input.val().trim(),
					completed: false,
					order: app.vocabularies.nextOrder()
				});
				app.vocabularies.add(newVocabulary);
				app.vocabulariesActive.add(newVocabulary);
				//call upload data to localStorage
				app.vocabularies.uploadLocalStorage();
				this.render(app.listView.collection);
				this.$input.val("");
				this.$input.focus();
			}
		},
		//Clear all vocabulary have "completed" attribute are true
		clearCompleted: function(){
			var i;
			for(i = 0; i < app.vocabulariesCompleted.length; i += 1){
				app.vocabularies.remove(app.vocabulariesCompleted.models[i]);
			}
			app.vocabulariesCompleted.reset(app.vocabularies.getCompletedItem());
			app.vocabularies.uploadLocalStorage();
			this.render(app.listView.collection);
		}
	});
	return AppView;
});




