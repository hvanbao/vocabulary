define([
	"jquery",
	"underscore",
	"backbone",
	//Pull in the view module from above
	"views/vocabularyView"
], function($, _, Backbone, VocabularyView){
	//This is View's vocabulary list
	var VocabularyListView = Backbone.View.extend({
		tagName: "tbody",
		id: "vocabylary-list-tbody",
		//Render list of vocabulary, $el = [<tr>...</tr>]
		render: function(){
			var that = this;
			//_.each function of underscore is synchronous
			_.each(that.collection.models, function(vocabulary){
				var vocabularyView = new VocabularyView({model: vocabulary});
				that.$el.append(vocabularyView.render().$el[0]); //calling render method manually
			});
			 //returning this for chaining..
			return this;
		}
	});
	return VocabularyListView;
});

