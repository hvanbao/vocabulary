define([
	"jquery",
	"underscore",
	"backbone",
	// Pull in the templates module from above
	"text!templates/vocabularyItemTemplate.html",
	"text!templates/editItemTemplate.html",
	// Pull in the Collection module from above
	"collections/vocabularyCollection"
], function($, _, Backbone, vocabularyItemTemplate, editItemTemplate){
	var VocabularyView = Backbone.View.extend({
		tagName: "tr",
		//Cache the template function for a single item.
		template: _.template(vocabularyItemTemplate),
		//template when you want to (double click) edit a vocabulary
		editTemplate: _.template(editItemTemplate),
		//The DOM events specific to an item.
		events: {
			"click .destroy": "deleteItem",
			"click .completed": "toggleCompleted",
			"keydown #vocabulary-input": "revertOnEscape",
			"dblclick #vocabulary": "editItem",
			"keypress #vocabulary-input": "updateSave",
			"blur #vocabulary-input": "close"
		},
		initialize: function(){
			this.listenTo(this.model, "destroy", this.remove);
		},
		//render
		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;  // returning this from render method..
		},
		//Toggle 'complete' attribute of this model
		toggleCompleted: function(){
			//call toggleCompletedState function in model of this View
			this.model.switchState();
			app.vocabularies.uploadLocalStorage();
			//Update collections which are used in application
			app.updateCollection();
			app.appView.render(app.listView.collection);
		},
		//Delete this model and re-render appView
		deleteItem: function(){
			this.model.destroy();
			//Update collections which are used in application
			app.vocabularies.uploadLocalStorage();
			app.appView.render(app.listView.collection);
		},
		//After you double in Vocabulary field,
		//this function will change template of this model
		editItem: function(){
			this.$el.html(this.editTemplate(this.model.toJSON()));
			$("#vocabulary-input").focus();
		},
		//Function, save vocabulary
		save: function(){
			var $newVocabulary = $("#vocabulary-input").val();
			console.log($newVocabulary);
			this.model.updateName($newVocabulary);
			app.vocabularies.uploadLocalStorage();
			this.$el.html(this.template(this.model.toJSON()));
			app.appView.render(app.listView.collection);
		},
		//After you double in Vocabulary field and change new vocabulary
		//And you click save, this function will run....
		updateSave: function(e){
			if(e.which === ENTER_KEY){
				this.save();
			}
		},
		//When you double and edit on name of Vocabulary but you don't
		//want to edit after, you can keydown ESC to revert this.
		revertOnEscape: function(e){
			if(e.which === ESC_KEY){
				$("#vocabulary-input").val(this.model.get("vocabulary"));
				this.$el.html(this.template(this.model.toJSON()));
				app.appView.render(app.listView.collection);
			}
		},
		//When lose focus in vocabulary input, close function will ran
		close: function(){
			var $newVocabulary = $("#vocabulary-input").val();
			if($newVocabulary.trim()){
				this.save();
			}
			else{
				this.model.destroy();
				//Update collections which are used in application
				app.vocabularies.uploadLocalStorage();
				app.appView.render(app.listView.collection);
			}
		}
	});
	return VocabularyView;
});

//This is View of vocabulary item

